const http = require('http');
const fetch = require('node-fetch');
const url = `https://s3-us-west-1.amazonaws.com/cdn-web.tunein.com/stations.json`;

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
  res.setHeader('Access-Control-Allow-Headers', '*');
  let settings = { method: 'Get' };

  fetch(url, settings)
    .then(res => res.json())
    .then(json => {
      res.end(JSON.stringify(json));
      console.log(json);
    });
  //res.end('started');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
