
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613933297/Screen_Shot_2021-02-21_at_8.47.49_PM_vm1hmq.png)

Mini TuneIn API that returns a list of stations:

https://s3-us-west-1.amazonaws.com/cdn-web.tunein.com/stations.json

- Be creative with this. We want to see a functional site that uses the api.

- The homepage of your app shows a list of available stations to play. You decide how you want to order the stations and how they are displayed.

- A user should be able to click on a station in the list

- Once a station is clicked, details about the station should be displayed and the associated stream should begin playing

- Be creative with tags. Can you help a user decide what to listen to based on tags?

- What can the reliability and popularity information associated with a station be used for?

- When in doubt, make an executive decision.

- Functionality is more important than the look and feel.

#Once finished, please send your solution files or a github link#

Some Tips

- Our web stack includes React, Redux, NodeJs and webpack but please use any languages, frameworks, or libraries you like. Pick something you are comfortable with. Whatever helps you show off your best coding skills!

- JSON Formatter chrome extension can be helpful while you’re formulating the architecture of your site.

- Assume you only need to only support modern evergreen browsers.

- Assume streams can be played with the html audio tag.


#### Good testing article ####
- https://medium.com/@dev.adrieltosi/testing-async-redux-actions-moxios-redux-mock-store-ee03abd04f1b
