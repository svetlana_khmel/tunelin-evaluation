import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import App from './App';
import { data, tags } from '../__test-data__/data';

import { makeMockStore } from '../__test-data__';

import Card from './components/card';

const store = makeMockStore({});

it('App renders correctly', () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <App data={data} />
      </Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('Card renders correctly', () => {
  const tree = renderer.create(<Provider store={store}><Card tags={tags} /></Provider>).toJSON();
  expect(tree).toMatchSnapshot();
});
