import moxios from 'moxios';
import {
  loadData, getData, sortBy, setTags, sortByTag,
} from './index';
import * as actions from './actionTypes';
import { makeMockStore } from '../__test-data__';

const store = makeMockStore({});

const mockSuccess = (data) => ({ status: 200, response: { data } });
const mockError = (error) => ({ status: 500, response: error });

describe('Actions', () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  it('Should create an action to load data ', () => {
    const data = { data: 'test' };
    const expectedAction = {
      type: actions.LOAD_DATA,
      payload: data,
    };

    expect(loadData(data)).toEqual(expectedAction);
  });

  it('Success API request calls getData acrion', () => {
    const data = { data: { tags: ['1', '2'] } };
    const tags = ['1', '2'];

    moxios.wait(() => {
      const request = moxios.request.mostRecent();
      request.respondWith(mockSuccess(data));
    });

    // Expected array of called actions

    const expected = [setTags(tags), loadData(data)];

    store.dispatch(getData()).then(() => {
      const actionCalled = store.getActions();
      expect(actionCalled).toEqual(expected);
    });
  });

  it('Failed API request calls getError acrion ', () => {
    // This functional is not implemented
    const errors = {
      message: 'error 500',
    };

    moxios.wait(() => {
      const request = moxios.request.mostRecent();
      request.respondWith(mockError(errors));
      const expected = [getErrors(erros)];
      store.displatch(getData()).then(() => {
        const actionsCalled = store.getActions();
        expect(actionsCalled).toEqual(expected);
      });
    });
  });
});
