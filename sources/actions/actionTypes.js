const LOAD_DATA = 'LOAD_DATA';
const SORT_BY = 'SORT_BY';
const SET_TAGS = 'SET_TAGS';
const SORT_BY_TAG = 'SORT_BY_TAG';

export {
  LOAD_DATA, SORT_BY, SET_TAGS, SORT_BY_TAG,
};
