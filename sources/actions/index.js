import axios from 'axios'; // axios has  been chosen instead of fetch because it needs for testing
import * as actionTypes from './actionTypes';

const url = 'http://127.0.0.1:3000/';

const loadData = (data) => ({ type: actionTypes.LOAD_DATA, payload: data });

const sortBy = (type) => ({ type: actionTypes.SORT_BY, payload: type });

const setTags = (tags) => ({ type: actionTypes.SET_TAGS, payload: tags });

const sortByTag = (tag) => ({ type: actionTypes.SORT_BY_TAG, payload: tag });

const getData = () => (dispatch) => axios.get(url)
  .then((resp) => {
    const { data } = resp;
    dispatch(loadData(data.data));

    const tags = data.data.reduce((acc, curVal) => [...acc, ...curVal.tags], []);

    const unicTags = tags.filter((v, i, a) => a.indexOf(v) === i);

    dispatch(setTags(unicTags));
  })
  .catch((error) => {
    console.log(error);
  });

export {
  loadData, getData, sortBy, setTags, sortByTag,
};
