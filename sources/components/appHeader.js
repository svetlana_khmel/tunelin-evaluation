import React from 'react';
import Button from '@material-ui/core/Button';
import Chips from './chips';

export default function AppHeader({ tags, sortBy, filterByTag }) {
  return (
    <div>
      <Button onClick={() => sortBy('popularity')}>By popularity</Button>
      <Button onClick={() => sortBy('reliability')}>By reliability</Button>
      <div>
        By tags:
        {' '}
        <Button
          onClick={() => {
            filterByTag('');
          }}
        >
          Show all
        </Button>
        <Chips tags={tags} filterByTag={filterByTag} component="header" />
      </div>
    </div>
  );
}
