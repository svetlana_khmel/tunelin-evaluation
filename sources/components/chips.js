import React, { memo } from 'react';

import './styles.scss';

const Chips = memo(({ tags, filterByTag, component }) => {
  tags = Array.isArray(tags) ? tags : tags.tags;
  return tags && Array.isArray(tags)
    ? tags.map((el, i) => (
      <div className="chip" key={el + i} onClick={component === 'header' ? () => filterByTag(el) : null}>
        {el}
      </div>
    ))
    : null;
});

export default Chips;
