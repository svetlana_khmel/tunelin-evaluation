import React, { Suspense } from 'react';
import ErrorBoundary from '../errorBoundary';
import Loader from '../loader';

const withSuspence = (Component) => function withSuspenceComponent({ error, ...props }) {
  return (
    <ErrorBoundary error={error}>
      <Suspense fallback={<Loader />}>
        <Component {...props} />
      </Suspense>
    </ErrorBoundary>
  );
};

export default withSuspence;
