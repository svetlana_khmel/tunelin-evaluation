import React, {useCallback} from 'react';
import { useSelector } from 'react-redux';

const Search = (query) => {
  const data = useSelector((state) => state.data);
  return useCallback((searchQuery) => data.filter((el) => el.name.toLowerCase().search(searchQuery.toLowerCase()) !== -1))
};


export default Search;