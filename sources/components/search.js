import React from 'react';

const SearchInput = ({ handleSearch, searchQuery }) => (<input onChange={handleSearch} type="text" value={searchQuery} />);

export default SearchInput;
