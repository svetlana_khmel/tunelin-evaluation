import React, { useState, useRef, memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';

import Chips from './chips';
import './styles.scss';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    margin: '3px',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

const MediaControlCard = memo(({
  imgUrl, name, description, tags, popularity, reliability, streamUrl,
}) => {
  const classes = useStyles();
  const [visibility, setVisibility] = useState('hidden');
  const audioEl = useRef(null);

  const switchCard = () => {
    setVisibility(`${visibility === 'hidden' ? 'shown' : 'hidden'}`);
    visibility === 'hidden' ? audioEl.current.play() : audioEl.current.stop();
  };

  return (
    <Card className={classes.root}>
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <div className="name" onClick={() => switchCard()}>
            <Typography component="h5" variant="h5">
              {name}
            </Typography>
          </div>
          <div className={`content ${visibility}`}>
            <img src={imgUrl} className="logo" alt="" className={classes.square} />
            <Typography variant="subtitle1" color="textSecondary">
              {description}
            </Typography>
            <Chip label={`Popularity: ${popularity}`} color="primary" />
            <Chip label={`Reliability: ${reliability}`} color="primary" />
            <div className={classes.controls}>
              <audio ref={audioEl} controls>
                <source src={streamUrl} type="audio/mpeg" />
              </audio>
            </div>
            <Chips tags={tags} />
          </div>
        </CardContent>
      </div>
    </Card>
  );
});

export default MediaControlCard;
