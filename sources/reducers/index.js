import * as actionTypes from '../actions/actionTypes';

const initialState = {
  sort: 'popularity',
  tag: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_DATA:
      return { ...state, data: action.payload };
    case actionTypes.SORT_BY:
      return { ...state, sort: action.payload };
    case actionTypes.SET_TAGS:
      return { ...state, tags: action.payload };
    case actionTypes.SORT_BY_TAG:
      return { ...state, tag: action.payload };

    default:
      return state;
  }
};

export default reducer;
