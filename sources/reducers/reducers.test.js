import reducer from './index';
import * as actions from '../actions/actionTypes';

describe('test reducers', () => {
  it('should return the initial state', () => {
    const expectedInitialState = {
      sort: 'popularity',
      tag: '',
    };
    expect(reducer(undefined, {})).toEqual(expectedInitialState);
  });

  it('should handle LOAD_DATA', () => {
    const payload = { id: 9775718 };
    const result = { data: { id: 9775718 } };

    expect(
      reducer({},
        {
          type: actions.LOAD_DATA,
          payload,
        }),
    ).toEqual(result);
  });

  it('should handle SORT_BY', () => {
    const payload = 'popularity';
    const expectedResult = { sort: 'popularity' };

    expect(reducer({},
      {
        type: actions.SORT_BY,
        payload,
      })).toEqual(expectedResult);
  });

  it('should handle SET_TAGS', () => {
    const payload = ['a', 'b'];
    const expectedResult = { tags: ['a', 'b'] };

    expect(
      reducer(
        {},
        {
          type: actions.SET_TAGS,
          payload,
        },
      ),
    ).toEqual(expectedResult);
  });

  it('should handle SORT_BY_TAG', () => {
    const payload = 'pop';
    const expectedResult = { tag: 'pop' };

    expect(
      reducer(
        {},
        {
          type: actions.SORT_BY_TAG,
          payload,
        },
      ),
    ).toEqual(expectedResult);
  });
});
