import React, {
  useEffect, useCallback, useState, lazy,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getData, sortBy, sortByTag } from './actions';
import withSuspense from './components/HOC/withSuspense';
import useSearch from './components/hooks/search';

import './styles.scss';

const Card = lazy(() => import('./components/card'));
const SearchInput = lazy(() => import('./components/search'));
const AppHeader = lazy(() => import('./components/appHeader'));

const AppHeaderWithSuspense = withSuspense(AppHeader);
const SearchInputWithSuspense = withSuspense(SearchInput);
const CardWithSuspense = withSuspense(Card);

const App = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.data);
  const sort = useSelector((state) => state.sort);
  const tags = useSelector((state) => state.tags);
  const tag = useSelector((state) => state.tag);
  const filterByTag = useSelector((state) => state.tag);
  const [searchQuery, setSearchQuery] = useState('');
  const searchResults = useSearch('');

  useEffect(() => {
    dispatch(getData());
  }, []);

  const handleSearch = (event) => {
    setSearchQuery(event.target.value);
  };

  const callSortByTag = useCallback((tag) => dispatch(sortByTag(tag)), []);

  const callSortBy = useCallback((type) => dispatch(sortBy(type)), []);

  const handleSorted = useCallback((data, type) => data.sort((a, b) => b[type] - a[type]));

  //const handleCachedSearch = useCallback((data, searchQuery) => data.filter((el) => el.name.toLowerCase().search(searchQuery.toLowerCase()) !== -1) )
  const handleCachedSearch = useSearch(searchQuery);

  const renderCards = () => {
    let sorted = [];

    if (!data) return <div className="progressIcon">Loading...</div>;

    if (sort === 'popularity') {
      sorted = handleSorted(data, 'popularity');
    }

    if (sort === 'reliability') {
      sorted = handleSorted(data, 'reliability');
    }

    if (tag !== '') {
      sorted = sorted.filter((el) => el.tags.includes(tag));
    }

    if (searchQuery !== '') {
      //sorted = handleCachedSearch(data, searchQuery);
      sorted = searchResults(searchQuery);
    }

    return sorted.map((el) => (
      <CardWithSuspense
        key={el.id}
        imgUrl={el.imgUrl}
        name={el.name}
        description={el.description}
        tags={el.tags}
        popularity={el.popularity}
        reliability={el.reliability}
        streamUrl={el.streamUrl}
        filterByTag={filterByTag}
      />
    ));
  };

  return (
    <div>
      {<SearchInputWithSuspense handleSearch={handleSearch} value={searchQuery} />}
      {tags ? <AppHeaderWithSuspense sortBy={callSortBy} tags={tags} filterByTag={callSortByTag} /> : null}
      {renderCards()}
    </div>
  );
};

export default App;
